#!/bin/sh
set -xe
wget -L https://github.com/ch32-rs/wlink/releases/download/nightly/wlink-linux-x64.tar.gz
mkdir -p ./wlink-tmp-dir
tar -xvf ./wlink-linux-x64.tar.gz -C ./wlink-tmp-dir
mv ./wlink-tmp-dir/wlink .
rm -rf ./wlink-tmp-dir
rm -f ./wlink-linux-x64.tar.gz
