#define MIBS_IMPL
#include "mibs.h"

Mibs_Default_Allocator allocator = mibs_make_default_allocator();

typedef enum {
    CMD_BUILD_EXAMPLES,
    CMD_FLASH,
    CMD_CONFIGURE,
} Command;
static const char* string_commands[] = {"build_examples", "flash", "configure"};

void help(void)
{
    mibs_log(MIBS_LL_INFO, "Available commands:\n");
    for (size_t i = 0; i < mibs_array_len(string_commands); i++) {
        mibs_log(MIBS_LL_INFO, "    -C=%s\n", string_commands[i]);
    }
}

#define CONFIG_PATH "build_config.h"

#ifdef CONFIGURED

#include CONFIG_PATH

#define EXAMPLES_PREFIX "./examples/"

bool build_example(char* path)
{
    Mibs_Cmd cmd = {0};

    // C -> ELF
    char* filename_elf = path + strlen(EXAMPLES_PREFIX);
    char* ext_c = ".c";
    char* ext_elf = ".elf";
    filename_elf = mibs_string_replace(&allocator, filename_elf, ext_c, ext_elf);
    mibs_cmd_append(&allocator, &cmd, XGCC);
    mibs_cmd_append(&allocator, &cmd, path);
    mibs_cmd_append(&allocator, &cmd, "-o", filename_elf);
    mibs_cmd_append(&allocator, &cmd,
        "-g",
        "-ffunction-sections",
        "-static-libgcc",
        "-march=rv32ec",
        "-mabi=ilp32e",
        "-nostdlib", "-I.",
        "-Wall", "-Wextra"
    );
    mibs_cmd_append(&allocator, &cmd, "-T./link.ld", "-Wl,--gc-sections", "-lgcc");
    bool result = mibs_run_cmd(&allocator, &cmd, MIBS_CMD_SYNC, NULL).ok;
    mibs_da_deinit(&allocator, &cmd);
    
    // ELF -> BIN
    char* filename_bin = path + strlen(EXAMPLES_PREFIX);
    char* ext_bin = ".bin";
    filename_bin = mibs_string_replace(&allocator, filename_bin, ext_c, ext_bin);
    mibs_cmd_append(&allocator, &cmd, XOBJCOPY);
    mibs_cmd_append(&allocator, &cmd, "-O", "binary", filename_elf, filename_bin);
    result |= mibs_run_cmd(&allocator, &cmd, MIBS_CMD_SYNC, NULL).ok;
    mibs_da_deinit(&allocator, &cmd);

    mibs_free(&allocator, filename_elf);
    mibs_free(&allocator, filename_bin);
    return result;
}

#define WLINK "./wlink"

bool flash(const char* bin, int addr)
{
    char addrbuf[0xff];
    memset(addrbuf, 0, sizeof(addrbuf));
    sprintf(addrbuf, "0x0%x", addr);
    Mibs_Cmd cmd = {0};
    mibs_cmd_append(&allocator, &cmd, WLINK);
    mibs_cmd_append(&allocator, &cmd, "flash", "--address", addrbuf, bin);

    return mibs_run_cmd(&allocator, &cmd, MIBS_CMD_SYNC, NULL).ok;
}

int main(int argc, char **argv)
{
    mibs_rebuild(&allocator, argc, argv);

    Mibs_Options options = mibs_options(&allocator, argc, argv);
    Mibs_Option_Value *command_opt = map_get(&options, "C");
    if (!command_opt) {
        mibs_log(MIBS_LL_ERROR, "No command provided\n");
        help();
        return 1;
    }
    if (!mibs_expect_option_kind(command_opt, "-C", MIBS_OV_STRING)) return 1;
    const char* command = command_opt->data.string;

    if (mibs_compare_cstr(command, string_commands[CMD_BUILD_EXAMPLES])) {
        if (!build_example(EXAMPLES_PREFIX "blink.c")) return 1;
    } else if (mibs_compare_cstr(command, string_commands[CMD_FLASH])) {
        Mibs_Option_Value *binary_opt = map_get(&options, "bin");
        if (!binary_opt) {
            mibs_log(MIBS_LL_ERROR, "No binary binary provided\n");
            return 1;
        }
        if (!mibs_expect_option_kind(binary_opt, "-bin", MIBS_OV_STRING)) return 1;
        const char* binary = binary_opt->data.string;

        int address = 0x08000000;

        Mibs_Option_Value *address_opt = map_get(&options, "addr");
        if (address_opt) {
            if (!mibs_expect_option_kind(address_opt, "-addr", MIBS_OV_STRING)) return 1;
            address = atoi(address_opt->data.string);
        }

        if (!flash(binary, address)) return 1;
    } else {
        mibs_log(MIBS_LL_ERROR, "No such command as %s\n", command);
        return 1;
    }

    map_deinit(&options);
    return 0;
}

#else

void generate_config_define(Mibs_String_Builder *config, const char* macro, const char* value)
{
    mibs_sb_append_cstr(&allocator, config, "#define");
    mibs_sb_append_cstr(&allocator, config, " ");
    mibs_sb_append_cstr(&allocator, config, macro);
    mibs_sb_append_cstr(&allocator, config, " ");
    mibs_sb_append_cstr(&allocator, config, value);
    mibs_sb_append_char(&allocator, config, MIBS_NL[0]);
}

int main(int argc, char** argv)
{
    mibs_rebuild(&allocator, argc, argv);

    Mibs_Options options = mibs_options(&allocator, argc, argv);
    Mibs_Option_Value *command_opt = map_get(&options, "C");
    if (!command_opt) {
        mibs_log(MIBS_LL_ERROR, "No command provided\n");
        help();
        return 1;
    }
    if (!mibs_expect_option_kind(command_opt, "-C", MIBS_OV_STRING)) return 1;
    const char* command = command_opt->data.string;

    if (mibs_compare_cstr(command, "configure")) {
        mibs_clear_file(CONFIG_PATH);

        Mibs_String_Builder config = {0};

        // xprefix must be in quotes
        char* xprefix = "\"riscv64-unknown-linux-gnu-\"";
        Mibs_Option_Value *xprefix_opt = map_get(&options, "xprefix");
        if (xprefix_opt) {
            if (!mibs_expect_option_kind(xprefix_opt, "-xprefix", MIBS_OV_STRING)) return 1;
            xprefix = (char*)xprefix_opt->data.string;
        }

        generate_config_define(&config, "XPREFIX", xprefix);
        generate_config_define(&config, "XGCC", "XPREFIX \"gcc\"");
        generate_config_define(&config, "XOBJDUMP", "XPREFIX \"objdump\"");
        generate_config_define(&config, "XOBJCOPY", "XPREFIX \"objcopy\"");

        mibs_sb_append_null(&allocator, &config);

        mibs_write_file(CONFIG_PATH, config.items);
        mibs_da_deinit(&allocator, &config);

        Mibs_Cmd cmd = {0};

        const char* configured = "mibs_configured";
        mibs_cmd_append(&allocator, &cmd, MIBS_CC, "-o", configured, __FILE__, "-DCONFIGURED");
        if (!mibs_run_cmd(&allocator, &cmd, MIBS_CMD_SYNC, NULL).ok) return 1;
    } else {
        mibs_log(MIBS_LL_ERROR, "No such command as %s\n", command);
        mibs_log(MIBS_LL_INFO, "Hint: the build system is unconfigured yet, so only -C=configure is available\n");
        return 1;
    }

    return 0;
}

#endif
